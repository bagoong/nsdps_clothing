define('FreeShippingBar'
, [
  'Cart.Summary.View'
  , 'FreeShippingBar.View'
  ]
, function (
  CartSummaryView
  , FreeShippingBarView
  )
{
  'use strict';

  return {
    mountToApp: function(application)
  {
    CartSummaryView.prototype.childViews.FreeShippingBar = function()
    {
      return new FreeShippingBarView({
        model: this.model
      })
    }
  }
  }
});
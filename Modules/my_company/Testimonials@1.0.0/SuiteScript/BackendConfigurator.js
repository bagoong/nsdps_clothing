define('BackendConfigurator'
, [
      'Configuration'
  ]
, function (
      config
  )
{
  'use strict';
  
  config.hosts = [
         {
            title:'United States'
         ,   currencies:[
               {
                  title:'American Dollars'
               ,   code:'USD'
               }
            ]
         ,   languages:[
               {
                  title:'English'
               ,   host:'phebesca.supportwebstore.com'
               ,   locale:'en_US'
               }
            ]
         }
         ,   {
            title:'Philippines'
         ,   currencies:[
               {
                  title:'Philippine Peso'
               ,   code:'PHP'
               }
            ]
         ,   languages:[
               {
                  title:'Filipino'
               ,   host:'ph.phebesca.supportwebstore.com'
               ,   locale:'tl_PH'
               }   
            ]
         }
      ]

});
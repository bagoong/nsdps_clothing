define('Testimonials'
, [
    'Testimonials.Router'
  , 'Testimonials.Carousel.View'
  , 'Home.View'
  , 'Backbone.CompositeView'
  , 'PluginContainer'
  , 'underscore'
  , 'Testimonials.Collection'
  , 'Testimonials.Translations'
  , 'SC.Configuration'
  ]
, function (
    Router
  , CarouselView
  , Home
  , BackboneCompositeView
  , PluginContainer
  , _
  , Collection
  , Translations
  , Configuration
  )
{
  'use strict';

  return {

    plugCarouselIntoView: function plugCarouselIntoView(View, application, afterSelector) {

      if (!View.prototype.visitChildren) {
        View.prototype.initialize = _.wrap(View.prototype.initialize, function wrap(fn) {
            fn.apply(this, _.toArray(arguments).slice(1));
            BackboneCompositeView.add(this);
        });
      }

      View.prototype.preRenderPlugins = View.prototype.preRenderPlugins || new PluginContainer();

      View.prototype.preRenderPlugins.install({
        name: 'Testimonials.Carousel'
      , execute: function execute($el) {
          $el.find(afterSelector)
            .after('<div data-view="Testimonials.Carousel"></div>');
        }
      });

      View.addExtraChildrenViews({
        'Testimonials.Carousel': function wrapperFunction() {
          return function() {
            return new CarouselView({
              application: application
            , collection: new Collection()

            });
          };
        }
      });

    }

  , mountToApp: function(application) {

      if (this.isEnabled())
      {

        this.plugCarouselIntoView(Home, application, '.home-slider-container');

        Translations.addTranslations();

        return new Router(application);

      }
    }

  , isEnabled: function isEnabled() {
      return Configuration.get('testimonials.isEnabled');
    }

  }
});
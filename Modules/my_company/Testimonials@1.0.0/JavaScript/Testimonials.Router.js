define('Testimonials.Router'
, [
    'Testimonials.Form.View'
  , 'Backbone'
  , 'Testimonials.Model'
  ]
, function TestimonialsFormRouter(
    FormView
  , Backbone
  , Model
  )
{
  'use strict';

  return Backbone.Router.extend({

    routes: {
      'testimonials/new': 'newTestimonial',
      'testimonials/new?*options': 'newTestimonial'
    }

  , initialize: function initialize(application) {
      this.application = application;
    }

  , newTestimonial: function newTestimonial() {
      var model = new Model();
      var view = new FormView({
        application: this.application
      , model: model
      });

      view.showContent();
    }
  });
});
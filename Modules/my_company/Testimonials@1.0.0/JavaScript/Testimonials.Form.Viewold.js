define('Testimonials.Form.View'
, [
    'testimonials_form.tpl'
  , 'Backbone'
  , 'Backbone.FormView'
  , 'jQuery'
  , 'GlobalViews.Message.View'
  ]
, function TestimonialsFormView(
    testimonialsFormTpl
  , Backbone
  , BackboneFormView
  , jQuery

  )
{
  'use strict';

  return Backbone.View.extend({

    showSuccessMessage: false
  , template: testimonialsFormTpl

  , title: _('New Testimonial').translate()

  , getBreadcrumbPages: function getBreadcrumbPages() {
      return [{
        text: this.title,
        href: '/testimonials/new'
      }];
    }

  , events: {
      'submit form': 'saveForm'
    }

  , initialize: function initialize() {
      BackboneFormView.add(this);
      this.model.on('sync', jQuery.proxy(this, 'showSuccess'));
    }
  
  , bindings: {
      '[name="text"]': 'text'
    , '[name="title"]': 'title'
    , '[name="writerName"]': 'writerName'
    , '[name="rating"]': 'rating'
    }

  , showSuccess: function showSuccess() {
      this.showConfirmationMessage(this.successMessage, true);
      }

  , successMessage: _('Thank you for your testimonial. We\'ll review and publish it soon.').translate()
    
  });
});
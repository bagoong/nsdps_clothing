define('Testimonials.Collection'
, [
    'Testimonials.Model'
  , 'Backbone.CachedCollection'
  , 'underscore'
  , 'Utils'
  ]
, function TestimonialsCollection(
    Model
  , BackboneCachedCollection
  , _
  , Util
  )
{
  'use strict'

  return BackboneCachedCollection.extend({
    url: Util.getAbsoluteUrl('services/Testimonials.Service.ss')
  , model: Model
  , parse: function parse(response) {
      return response.records;
    }
  });
});
define('Testimonials.Translations'
, [
    'underscore'
  ]
, function (
    _
  )
{
  'use strict';

  return  {
    addTranslations: function addTranslations() {
      var locale = SC.ENVIRONMENT.currentLanguage.locale
      , dictionary = SC.Translations;

      if (locale === 'en_US')
      {
        _.extend(dictionary,
        {
          "What our customers say": "What our customers say"
        , "Leave yours": "Leave yours"
        , "Thank you for your testimonial. We\'ll review and publish it soon.": "Thank you for your testimonial. We\'ll review and publish it soon."
        , "Title": "Title"
        , "Write your testimonial": "Write your testimonial"
        , "New Testimonial": "New Testimonial"
        });
      }
      else if (locale === 'tl_PH')
//      debugger;  
      {
        _.extend(dictionary,
        {
          "What our customers say": "Anong sabi ni Duterte?"
        , "Leave yours": "Kay De Lima"
        , "Thank you for your testimonial. We\'ll review and publish it soon.": "Nous vous remercions de votre témoignage. Nous allons passer en revue et de le publier bientôt."
        , "Title": "Titre"
        , "Write your testimonial": "Magsulat ka!"
        , "New Testimonial": "Bagong Testi"
        });
      }
    }
  }
});
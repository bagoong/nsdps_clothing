{{#if isReadyToRender}}
<section class="testimonials-carousel">
    <h1 class="testimonials-carousel-title">{{translate 'What our customers say'}}</h1><a href="/testimonials/new">{{translate 'Leave yours'}}</a>
    <div data-slider data-view="Testimonials.Collection"></div>
</section>
{{/if}}
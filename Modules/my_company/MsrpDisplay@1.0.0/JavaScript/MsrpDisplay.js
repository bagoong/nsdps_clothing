define('MsrpDisplay'
, [
    'Backbone'
  , 'SC.Configuration'
  , 'underscore'
  , 'ItemDetails.Model'
  , 'ItemViews.Price.View'
  ]
, function
  (
    Backbone
  , Configuration
  , _
  , ItemDetailsModel
  , ItemViewsPriceView
  )
{
  'use strict';

  return {
    mountToApp: function(application)
    {
      Configuration.itemKeyMapping = Configuration.itemKeyMapping || {};
      _.extend(Configuration.itemKeyMapping,
      {
        _retailPrice: function _retailPrice(item)
        {
          return item.get('pricelevel9');
        }
      , _retailPriceFormatted: function _retailPriceFormatted(item)
        {
          return item.get('pricelevel9_formatted');
        }
      });

      ItemDetailsModel.prototype.getRetailPrice = function getRetailPrice()
      {
        var price_retail = this.get('_retailPrice')
        , price_retail_formatted = this.get('_retailPriceFormatted');

        return {
          unformatted: price_retail
        , formatted: price_retail_formatted
        };
      }
      
      ItemViewsPriceView.prototype.getContext = _.wrap(ItemViewsPriceView.prototype.getContext, function(fn)
      {
        var context = fn.apply(this, _.toArray(arguments).slice(1));

        context.priceRetail = this.model.getRetailPrice().unformatted;
        context.priceRetailFormatted = this.model.getRetailPrice().formatted;

        return context;
      });  
    }
  }
});